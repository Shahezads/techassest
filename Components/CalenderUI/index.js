"use client";
import * as React from "react";
import { NoSsr } from "@mui/material";
import { Blocked, Reservations } from "../StaticJSON";
import { DateRangePicker } from "rsuite";
import "rsuite/DateRangePicker/styles/index.css";
import { isEmpty } from "lodash";
import moment from "moment";

export default function CalenderUI(props) {
  const { value, setValue, timeZone } = props;

  // combine data for blocked and reserved dates
  const disableddata = Reservations.concat(Blocked);

  // function for disabled date whhich are reserved or booked
  const disabledDate = (date) => {
    const dateString = moment(date).format("YYYY-MM-DD");
    const getHourss = moment(date).hours();
    // const reservedPeriods = disableddata
    //   .filter((reservation) => {
    //     return (
    //       dateString >= moment(reservation.start_date).format("YYYY-MM-DD") &&
    //       dateString <= moment(reservation.end_date).format("YYYY-MM-DD")
    //     );
    //   })
    //   .map((reservation) => reservation.start_period);

    for (const reservation of disableddata) {
      const startDateString = reservation.start_date;
      const endDateString = reservation.end_date;

      const isSameDayAsStart = dateString === startDateString;
      const isSameDayAsEnd = dateString === endDateString;

      const startPeriod = reservation.start_period;
      const endPeriod = reservation.end_period;

      // let Speriod = startDateString === 1 ? 8 : 12;
      // let Eperiod = endDateString === 1 ? 8 : 12;

      if (
        ((dateString <= startDateString && dateString >= startDateString) ||
          (dateString <= endDateString && dateString >= endDateString) ||
          (dateString >= startDateString && dateString <= endDateString)) &&
        // Check if it's not the last day's evening
        !(isSameDayAsEnd && getHourss >= endPeriod) &&
        // Check if it's not the first day's morning
        !(isSameDayAsStart && getHourss < startPeriod)
      ) {
        return true; // Disable the date if it is within the reservation period
      }
    }

    return false; // Enable the date if it is not within any reservation period
  };

  const setStartDateWithAM = (startDate) => {
    // Set the time portion to 8:00 AM
    if (timeZone === "AM") {
      startDate.setHours(8, 0, 0, 0);
    } else {
      startDate.setHours(20, 0, 0, 0);
    }
    return startDate;
  };

  const setEndDateWithAM = (endDate) => {
    // Set the time portion to 8:00 AM
    if (timeZone === "AM") {
      endDate.setHours(8, 0, 0, 0);
    } else {
      endDate.setHours(20, 0, 0, 0);
    }
    return endDate;
  };

  const handleDateChange = (newDateRange) => {
    // Ensure the time portion is set to 8:00 AM for both start and end dates

    if (!isEmpty(newDateRange)) {
      setValue([
        setStartDateWithAM(newDateRange[0]),
        setEndDateWithAM(newDateRange[1]),
      ]);
    } else {
      setValue([]);
    }
  };

  return (
    <NoSsr>
      <DateRangePicker
        // defaultOpen={true}
        placeholder="Select start and end date:"
        style={{ width: "100%" }}
        shouldDisableDate={disabledDate}
        // disabledDate={disabledDate}
        // shouldDisableHour={disabledHours}
        value={value}
        onChange={handleDateChange}
        showMeridian
        showOneCalendar
        format="dd-MM-yyyy"
        defaultCalendarValue={value}
      />
    </NoSsr>
  );
}
