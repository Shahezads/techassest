export const Reservations = [
  {
    start_date: "2024-01-31",
    end_date: "2024-02-01",
    start_period: 2,
    end_period: 1,
  },
  {
    start_date: "2024-02-01",
    end_date: "2024-02-01",
    start_period: 1,
    end_period: 2,
  },
  {
    start_date: "2024-02-03",
    end_date: "2024-02-04",
    start_period: 1,
    end_period: 1,
  },
  {
    start_date: "2024-02-19",
    end_date: "2024-02-19",
    start_period: 1,
    end_period: 2,
  },
  {
    start_date: "2024-03-01",
    end_date: "2024-03-10",
    start_period: 1,
    end_period: 2,
  },
];

export const Blocked = [
  {
    start_date: "2024-02-15",
    end_date: "2024-02-16",
    start_period: 1,
    end_period: 1,
  },
  {
    start_date: "2024-01-27",
    end_date: "2024-01-30",
    start_period: 2,
    end_period: 2,
  },
  {
    start_date: "2024-02-21",
    end_date: "2024-02-24",
    start_period: 1,
    end_period: 2,
  },
];

export const Prices = [
  {
    start_date: "2024-01-01",
    end_date: "2024-01-31",
    price: {
      weekday: {
        morning: 50,
        evening: 70,
      },
      weekend: {
        morning: 80,
        evening: 120,
      },
    },
  },
  {
    start_date: "2024-02-01",
    end_date: "2024-02-29",
    price: {
      weekday: {
        morning: 60,
        evening: 80,
      },
      weekend: {
        morning: 100,
        evening: 150,
      },
    },
  },
  {
    start_date: "2024-03-01",
    end_date: "2024-03-31",
    price: {
      weekday: {
        morning: 55,
        evening: 75,
      },
      weekend: {
        morning: 85,
        evening: 125,
      },
    },
  },
  {
    start_date: "2024-04-01",
    end_date: "2024-04-30",
    price: {
      weekday: {
        morning: 30,
        evening: 40,
      },
      weekend: {
        morning: 35,
        evening: 45,
      },
    },
  },
];

export const specialPrices = [
  {
    start_date: "2024-01-28",
    end_date: "2024-02-04",
    price: {
      weekday: {
        morning: 200,
        evening: 250,
      },
      weekend: {
        morning: 280,
        evening: 320,
      },
    },
  },
  {
    start_date: "2024-02-07",
    end_date: "2024-02-08",
    price: {
      weekday: {
        morning: 10,
        evening: 15,
      },
      weekend: {
        morning: 10,
        evening: 15,
      },
    },
  },
  {
    start_date: "2024-02-11",
    end_date: "2024-02-15",
    price: {
      weekday: {
        morning: 155,
        evening: 175,
      },
      weekend: {
        morning: 185,
        evening: 225,
      },
    },
  },
  {
    start_date: "2024-03-01",
    end_date: "2024-03-10",
    price: {
      weekday: {
        morning: 300,
        evening: 400,
      },
      weekend: {
        morning: 350,
        evening: 450,
      },
    },
  },
];
