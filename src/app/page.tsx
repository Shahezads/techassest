"use client";
import * as React from "react";
import CalenderUI from "../../Components/CalenderUI";
import { Box, Button, Divider, Grid, Typography } from "@mui/material";
import { isEmpty } from "lodash";
import moment from "moment";
import {
  Blocked,
  Prices,
  Reservations,
  specialPrices,
} from "../../Components/StaticJSON";
import { DarkMode, LightMode } from "@mui/icons-material";

export default function Home() {
  const [value, setValue] = React.useState<any>([]);
  const [dayDifference, setDayDifference] = React.useState<any>(0);
  const [pricesDays, setPricesDays] = React.useState<any>();
  const [specialPricesDays, setSpecialPricesDays] = React.useState<any>();
  const [timeZone, setTimeZone] = React.useState<any>("AM");
  const [minusDisable, setMinusDisable] = React.useState<any>(false);
  const [plusDisable, setPlusDisable] = React.useState<any>(false);

  const expensiveCalculation = (value: any) => {
    return value;
  };

  // get difference of selected date range.
  const diffDaysFunc = (value: any) => {
    // Calculate the time difference in milliseconds
    var diffTime: any = !isEmpty(value) && value[1] - value[0];
    // Convert milliseconds to days
    setDayDifference(diffTime / (1000 * 60 * 60 * 24));
  };

  // get normal price data from selected date range
  const getprices = () => {
    const pricesData =
      !isEmpty(value) &&
      Prices.filter(
        (fl) =>
          value[0] >= new Date(fl?.start_date) &&
          value[1] <= new Date(fl?.end_date)
      );
    setPricesDays(pricesData);
  };

  // get special price data from selected date range
  const getSpecialPrices = () => {
    const pricesData =
      !isEmpty(value) &&
      specialPrices.filter(
        (fl) =>
          value[0] >= new Date(fl?.start_date) &&
          value[1] <= new Date(fl?.end_date)
      );
    setSpecialPricesDays(pricesData);
  };

  const calculation = React.useMemo(
    () => expensiveCalculation(value),
    [value, dayDifference]
  );

  // merge two arrays for block and reserved dates
  const disableddata = Reservations.concat(Blocked);

  // Check if the date is in the list of disabled dates
  const shouldDisableDate = (date: any) => {
    return disableddata.some((disabledDate: any) => {
      let sDate = new Date(disabledDate.start_date);
      let eDate = new Date(disabledDate.end_date);

      return (
        (sDate.getFullYear() === date.getFullYear() ||
          eDate.getFullYear() === date.getFullYear()) &&
        (sDate.getMonth() === date.getMonth() ||
          eDate.getMonth() === date.getMonth()) &&
        (sDate.getDate() === date.getDate() + 1 ||
          eDate.getDate() === date.getDate() + 1)
      );
    });
  };

  // for add 1 date from selected date range
  const handleButtonPlusClick = () => {
    const dateDi = shouldDisableDate(calculation[1]);
    if (!dateDi) {
      setPlusDisable(false);
      setMinusDisable(false);
      // Get the current start date and set a new end date
      const curretDate = calculation[1];
      const newEndDate = curretDate
        ? new Date(curretDate.getTime() + 24 * 60 * 60 * 1000)
        : null;

      // Update the date range with the new end date
      setValue([calculation[0], newEndDate]);
    } else {
      setPlusDisable(true);
    }
  };

  // for remove 1 date from selected date range
  const handleButtonMinusClick = () => {
    const dateDi = shouldDisableDate(calculation[0]);
    if (!dateDi) {
      setPlusDisable(false);
      setMinusDisable(false);
      // Get the current end date and set a new end date
      const curretDate = calculation[1];
      const newEndDate = curretDate
        ? new Date(curretDate.getTime() - 24 * 60 * 60 * 1000)
        : null;

      // Update the date range with the new end date
      setValue([calculation[0], newEndDate]);
    } else {
      setMinusDisable(true);
    }
  };

  React.useEffect(() => {
    diffDaysFunc(value);
    getprices();
    getSpecialPrices();
  }, [value]);

  // for select price from selected dates
  let finalPrice = !isEmpty(specialPricesDays)
    ? specialPrices[0]
    : !isEmpty(pricesDays)
    ? pricesDays[0]
    : 0;

  // for display price on button
  let displayprice =
    !isEmpty(value) && timeZone === "AM"
      ? finalPrice?.price?.weekday?.morning * dayDifference
      : finalPrice?.price?.weekday?.evening * dayDifference || "";

  function getWeekendDays(startDate: any, endDate: any) {
    const weekends = [];
    let currentDay = moment(startDate);

    while (currentDay.isSameOrBefore(endDate)) {
      if (currentDay.day() === 0 || currentDay.day() === 6) {
        // 0 represents Sunday, 6 represents Saturday
        weekends.push(currentDay.clone().toDate());
      }
      currentDay.add(1, "day");
    }

    return weekends;
  }

  const weekendDays =
    !isEmpty(value) && getWeekendDays(calculation[0], calculation[1]);
  console.log("🚀 ~ Home ~ weekendDays:", weekendDays);

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        height: "calc(100vh)",
        alignItems: "flex-start",
        width: "100%",
        backgroundColor: "#fff",
      }}
    >
      <Grid container spacing={2} sx={{ maxWidth: 400, m: 0, width: "100%" }}>
        <Grid item xs={12}>
          <CalenderUI value={value} setValue={setValue} timeZone={timeZone} />
        </Grid>
        <Grid
          item
          xs={12}
          sx={{ display: "flex", alignItems: "center", width: "100%" }}
        >
          <Button
            fullWidth
            size="small"
            variant={timeZone === "AM" ? "contained" : "outlined"}
            sx={{
              backgroundColor:
                timeZone === "AM" ? "#44A9F7 !important" : "#fff",
              color: timeZone === "AM" ? "#fff" : "#9B9B9B",
              mr: 1,
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
            onClick={() => {
              setValue([]);
              setTimeZone("AM");
            }}
          >
            <Box>
              Check In
              <Typography>08:00 AM</Typography>
            </Box>
            <LightMode />
          </Button>
          <Button
            size="small"
            fullWidth
            variant={timeZone === "PM" ? "contained" : "outlined"}
            sx={{
              backgroundColor:
                timeZone === "PM" ? "#44A9F7 !important" : "#fff",
              color: timeZone === "PM" ? "#fff" : "#9B9B9B",
              ml: 1,
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
            onClick={() => {
              setValue([]);
              setTimeZone("PM");
            }}
          >
            <Box>
              Check In
              <Typography>08:00 PM</Typography>
            </Box>
            <DarkMode />
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Divider />
        </Grid>
        {!isEmpty(calculation) && (
          <>
            <Grid
              item
              xs={12}
              sx={{
                display: "flex",
                alignItems: "center",
                width: "100%",
                justifyContent: "space-between",
              }}
            >
              <Box sx={{ width: "50%" }}>Duration</Box>
              <Box
                sx={{
                  border: "1px solid #44A9F7",
                  width: "48%",
                  borderRadius: 1,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  color: "#44A9F7",
                }}
              >
                <Button
                  variant="contained"
                  sx={{
                    backgroundColor: minusDisable
                      ? "#ccc"
                      : "#44A9F7 !important",
                    // color: "#fff",
                    textAlign: "center",
                  }}
                  disabled={minusDisable}
                  onClick={() => {
                    if (dayDifference < 1) {
                      setMinusDisable(true);
                    } else {
                      handleButtonMinusClick();
                    }
                  }}
                >
                  -
                </Button>
                {dayDifference || ""}
                <Button
                  sx={{
                    backgroundColor: plusDisable
                      ? "#ccc"
                      : "#44A9F7 !important",
                    // color: "#fff",
                    textAlign: "center",
                  }}
                  variant="contained"
                  disabled={plusDisable}
                  onClick={() => handleButtonPlusClick()}
                >
                  +
                </Button>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Divider />
            </Grid>
          </>
        )}
        <Grid item xs={12}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            Check In
            <Typography sx={{ color: "#44A9F7" }}>
              {!isEmpty(calculation)
                ? moment(calculation[0]).format("MMMM Do YYYY, h:mm a")
                : "-"}
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            Check Out
            <Typography sx={{ color: "#44A9F7" }}>
              {!isEmpty(calculation)
                ? moment(calculation[1]).format("MMMM Do YYYY, h:mm a")
                : "-"}
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Button
            disabled={dayDifference < 1}
            fullWidth
            variant="contained"
            sx={{
              backgroundColor:
                dayDifference < 1 ? "#fff" : "#44A9F7 !important",
              color: dayDifference < 1 ? "#9B9B9B" : "#fff",
            }}
          >
            {`Price ${displayprice}`}
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}
